# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Task

* Ios: Please see file UI-for-ios.jpg for UI
* Android: Please see this [Link](https://imgur.com/R7HMGxk) for UI
* Make UI based on that link
* For the api you can use something like [this](https://www.tvmaze.com/api) or any api that you can used to display list of movies, list of actors etc
* If there is no api not suitable, please use hard code data 
* Feel free to be creative, UI illustration doesnt have to be exactly the same

### Language

* For android can use Java / Kotlin
* For IOS swift

### Contribution guidelines

* Please push your code to bitbucket with public mode ( open for all ) 
* For ios invite rogers@campaign.com and nupi@campaign.com
* Thanks and good luck
